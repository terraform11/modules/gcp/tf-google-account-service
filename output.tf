output "name" {
    value       = "${google_service_account.default.name}"
    description = "The fully-qualified name of the service account."
}

output "private_key" {
    value = "${google_service_account_key.default.private_key}"
}