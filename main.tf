resource "random_pet" "prefix" {
}

resource "random_id" "service_account" {
  byte_length = 4
  prefix      = "${random_pet.prefix.id}-"
}

resource "google_service_account" "default" {
    account_id   = lower(random_id.service_account.hex)
    display_name = "${var.display_name}"
}

resource "google_project_iam_binding" "default" {
    count = var.count-sa
    project = var.name
    role    =  var.role[count.index]
    members  = ["serviceAccount:${google_service_account.default.email}"]
}

resource "google_service_account_key" "default" {
    depends_on          = ["google_service_account.default"]
    service_account_id  = "${google_service_account.default.name}"
}