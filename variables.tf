variable "display_name" {
    description = "The display name for the service account. Can be updated without creating a new resource."
}

variable "role" {
    description = "The roles that will be granted to the account."
    type = list(string)
}

variable "name" {
    description = "The project name."
}

variable "count-sa" {
    description = "Count."
    type = number
}